package com.brihaspati.freedrinks.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.brihaspati.freedrinks.R;
import com.brihaspati.freedrinks.pojo.MessagesPojo;
import com.brihaspati.freedrinks.pojo.socialpostpojo;

import java.util.List;

/**
 * Created by brst-pc97 on 3/27/17.
 */
public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.MyViewHolder> {

    private List<MessagesPojo> list;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView username, timeAgo, post;
        ImageView userImage, socialImage;

        public MyViewHolder(View view) {
            super(view);
            username = (TextView) view.findViewById(R.id.text_name);
            timeAgo = (TextView) view.findViewById(R.id.text_time);
            post = (TextView) view.findViewById(R.id.text_post);
        }
    }


    public MessagesAdapter(List<MessagesPojo> moviesList) {
        this.list = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_messages, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MessagesPojo postpojo = list.get(position);
            holder.username.setText(postpojo.getName());
        holder.timeAgo.setText(postpojo.getTime());
        holder.post.setText(postpojo.getText());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}
