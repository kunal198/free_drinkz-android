package com.brihaspati.freedrinks.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brihaspati.freedrinks.R;
import com.brihaspati.freedrinks.pojo.socialpostpojo;
import com.brihaspati.freedrinks.ui.adapters.SocialPostsAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brst-pc97 on 3/27/17.
 */
public class ProfileFragment extends Fragment {

    public  static  ProfileFragment fragmentInstance = null;

    public  static  String TAG = "ProfileFragment";

    View view = null;
    RecyclerView recyclerView;
    SocialPostsAdapter adapter = null;
    Context context;
    List<socialpostpojo>  list = null;

    public static ProfileFragment getInstance(Context context){


        try {
            context = context;
            if(fragmentInstance == null){
                fragmentInstance = new ProfileFragment();
            }
            Log.i(TAG, "Created");
            return fragmentInstance;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.i(TAG,"onCreateView");
        try {
            view = inflater.inflate(R.layout.profile_fragment, null);
            initViews();
            setUpViews();
            return view;
        }catch (Exception e){
            e.printStackTrace();
        }


        return view;
    }

    private void initViews() {

        try {
            recyclerView =(RecyclerView) view.findViewById(R.id.recycler_view_user_post);
        }catch (Exception e){
            e.getCause();
        }

    }


    private void setUpViews(){

        try {

            list = new ArrayList<>();

            adapter = new SocialPostsAdapter(list);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
            prepareData();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void prepareData(){

        try {

             for (int i = 0; i<10; i++ ){
                 socialpostpojo pojo = new socialpostpojo();

                 pojo.setUsername("Martin");
                 pojo.setPostText("Lorem text");
                 pojo.setPostType("Facebook");
                 pojo.setTimeAgo("9 hours ago");
                 pojo.setProfilePicUrl("");

                 list.add(pojo);

             }

            adapter.notifyDataSetChanged();

        }catch (Exception e){
            e.getStackTrace();
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.i(TAG,"onActivityCreated");
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG,"onResume");
    }


    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG,"onPause");
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG,"onDestroy");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG,"onDistroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG,"onDetach");
    }
}
