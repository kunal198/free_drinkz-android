package com.brihaspati.freedrinks.ui.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brihaspati.freedrinks.R;
import com.brihaspati.freedrinks.ui.baseactivities.BaseActivity;
import com.brihaspati.freedrinks.utilities.ImagePicker;
import com.brihaspati.freedrinks.utilities.Utilites;

import java.util.ArrayList;

/**
 * Created by brst-pc97 on 3/24/17.
 */
public class SignUpActivity extends BaseActivity {

    Toolbar mToolbarLogin;
    EditText mEdFirstName ,mEdLastName, mEdEmail, mEdPassword, mEdConfirmPassword, mEdState, mEdCity, mEdZip;
    android.support.v7.widget.AppCompatSpinner mSpinnerAge, mSpinnerSex;
    TextView  mToolBarTittle;
    String[] SpineerListSex = {"Male","Female"};
    ArrayList<String> ageList = new ArrayList<>();
    ArrayAdapter<String> arrayAdapterAge;
    ArrayAdapter<String> arrayAdapterSex;
    com.mikhaellopez.circularimageview.CircularImageView mIvUserProfilePic;
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        setUpViews();
        setUpData();
    }

    @Override
    public void setUpViews() {

        try {
            mToolbarLogin = (Toolbar)findViewById(R.id.toolbar);

            mIvUserProfilePic = (com.mikhaellopez.circularimageview.CircularImageView)findViewById(R.id.image_user);

            mEdFirstName  = (EditText)findViewById(R.id.edit_text_first_name);
            mEdLastName  = (EditText)findViewById(R.id.edit_last_name);
            mEdEmail    = (EditText)findViewById(R.id.edit_text_email);
            mEdPassword = (EditText)findViewById(R.id.edit_text_password);
            mEdConfirmPassword  = (EditText)findViewById(R.id.edit_text_confirm_password);
            mEdState  = (EditText)findViewById(R.id.edit_text_state);
            mEdCity  = (EditText)findViewById(R.id.edit_text_city);
            mEdZip  = (EditText)findViewById(R.id.edit_text_zip_code);
            mSpinnerAge = (android.support.v7.widget.AppCompatSpinner)findViewById(R.id.spinner_age);
            mSpinnerSex = (android.support.v7.widget.AppCompatSpinner)findViewById(R.id.spinner_gender);


            mToolBarTittle = (TextView)findViewById(R.id.text_toolbar_title);
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    @Override
    public void setUpData() {

        try {

            for(int i = 1; i<= 100; i++){
                ageList.add(i+"");
            }

            arrayAdapterAge = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_dropdown_item, ageList);

            arrayAdapterSex = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_dropdown_item, SpineerListSex);

            mSpinnerAge.setAdapter(arrayAdapterAge);
            mSpinnerSex.setAdapter(arrayAdapterSex);

            setSupportActionBar(mToolbarLogin);
            mToolBarTittle.setText("Sign up");

        }catch (Exception e){
            e.getStackTrace();
        }


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.btn_register:
                Intent I = null;
                    if(Utilites.isInternetOn(this)){

                        I = new Intent(this, SignupSecondActivity.class);
                        I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(I);
                        finish();
                    }

                break;

            case R.id.layout_signin:

                if(Utilites.isInternetOn(this)){


                    I = new Intent(this, LoginActivity.class);
                    I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(I);
                    finish();

                }

                break;

            case R.id.layout_image_picker:

                  onPickImage();

                break;


        }

    }

    //Image Picker
    public void onPickImage() {
        try {
            Intent chooseImageIntent = ImagePicker.getPickImageIntent(getApplicationContext());
            startActivityForResult(chooseImageIntent, 234);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

                Uri selectedImageURI = null;
                if (data != null) {
                    selectedImageURI = data.getData();
                }
                if (selectedImageURI != null) {
                    mIvUserProfilePic.setImageURI(null);
                    mIvUserProfilePic.setImageURI(selectedImageURI);
                } else {

                    Bitmap bitmap = ImagePicker.getImageFromResult(getApplicationContext(), resultCode, data);
                    if (bitmap != null) {
                        mIvUserProfilePic.setImageBitmap(bitmap);
                    }
                }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
