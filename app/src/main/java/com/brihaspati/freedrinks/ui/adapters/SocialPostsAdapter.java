package com.brihaspati.freedrinks.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.brihaspati.freedrinks.R;
import com.brihaspati.freedrinks.pojo.socialpostpojo;

import java.util.List;

/**
 * Created by brst-pc97 on 3/27/17.
 */
public class SocialPostsAdapter extends RecyclerView.Adapter<SocialPostsAdapter.MyViewHolder> {

    private List<socialpostpojo> list;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView username, timeAgo, post;
        ImageView userImage, socialImage;

        public MyViewHolder(View view) {
            super(view);
            username = (TextView) view.findViewById(R.id.text_view_post_user_name);
            timeAgo = (TextView) view.findViewById(R.id.text_view_post_time);
            post = (TextView) view.findViewById(R.id.text_view_post);
        }
    }


    public SocialPostsAdapter(List<socialpostpojo> moviesList) {
        this.list = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.profile_post_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        socialpostpojo postpojo = list.get(position);
        holder.username.setText(postpojo.getUsername());
        holder.timeAgo.setText(postpojo.getTimeAgo());
        holder.post.setText(postpojo.getPostText());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
