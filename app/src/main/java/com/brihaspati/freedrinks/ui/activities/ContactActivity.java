package com.brihaspati.freedrinks.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.brihaspati.freedrinks.R;
import com.brihaspati.freedrinks.ui.baseactivities.BaseActivity;
import com.brihaspati.freedrinks.utilities.Utilites;

/**
 * Created by brst-pc97 on 3/27/17.
 */
public class ContactActivity extends BaseActivity {

    private Toolbar toolbar;
    TextView mToolBarTittle;
    @Override
    protected void onCreate( Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        setUpViews();
        setUpData();
    }

    @Override
    public void setUpViews() {
        // Set a Toolbar to replace the ActionBar.
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolBarTittle = (TextView)findViewById(R.id.text_toolbar_title);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
        mToolBarTittle.setText("Contact");

    }

    @Override
    public void setUpData() {

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.btn_send:

                if(Utilites.isInternetOn(this)){
                    Toast.makeText(ContactActivity.this,"Submitted",Toast.LENGTH_SHORT).show();

                }

                break;

        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case android.R.id.home:
                onBackPressed();
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
