package com.brihaspati.freedrinks.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.brihaspati.freedrinks.R;
import com.brihaspati.freedrinks.ui.baseactivities.BaseActivity;

/**
 * Created by brst-pc97 on 3/27/17.
 */
public class PaymentActivity extends BaseActivity {
  Toolbar mToolBar;
    TextView  mToolBarTittle;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        setUpViews();
        setUpData();

    }

    @Override
    public void setUpViews() {

        try {
            mToolBar = (Toolbar)findViewById(R.id.toolbar);
            setSupportActionBar(mToolBar);
            mToolBarTittle.setText("Payment");

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);

        }catch (Exception e){
            e.getStackTrace();
        }

    }

    @Override
    public void setUpData() {

        try {

        }catch (Exception e){
            e.getStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.layout_paypal:

                break;

            case R.id.layout_credit_card:

                break;



        }

    }
}
