package com.brihaspati.freedrinks.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brihaspati.freedrinks.R;
import com.brihaspati.freedrinks.ui.baseactivities.BaseActivity;
import com.brihaspati.freedrinks.utilities.Utilites;

/**
 * Created by brst-pc97 on 3/24/17.
 */
public class SignupSecondActivity extends BaseActivity {

    Toolbar mToolbarLogin;
    TextView  mToolBarTittle;
    ImageView mIvMembers, mIvPlaces, mIvArtist;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_second);
        setUpViews();
        setUpData();
    }

    @Override
    public void setUpViews() {

        try {

            mToolbarLogin = (Toolbar)findViewById(R.id.toolbar);
            mToolBarTittle = (TextView)findViewById(R.id.text_toolbar_title);

            mIvMembers = (ImageView)findViewById(R.id.select_member);
            mIvPlaces = (ImageView)findViewById(R.id.select_places);
            mIvArtist = (ImageView)findViewById(R.id.select_musical_artist);


        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void setUpData() {

        try {

            setSupportActionBar(mToolbarLogin);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
            mToolBarTittle.setText("Sign up");

        }catch (Exception e){
            e.getStackTrace();
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.btn_next:
                Intent I = null;
                if(Utilites.isInternetOn(this)){

                    I = new Intent(this, MainActivity.class);
                    I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(I);
                    finish();
                }

                break;

            case R.id.select_member:

                mIvMembers.setImageResource(R.drawable.rd);
                mIvPlaces.setImageResource(R.drawable.rd2);
                mIvArtist.setImageResource(R.drawable.rd2);

                break;


            case R.id.select_places:
                mIvMembers.setImageResource(R.drawable.rd2);
                mIvPlaces.setImageResource(R.drawable.rd);
                mIvArtist.setImageResource(R.drawable.rd2);

                break;

            case R.id.select_musical_artist:
                mIvMembers.setImageResource(R.drawable.rd2);
                mIvPlaces.setImageResource(R.drawable.rd2);
                mIvArtist.setImageResource(R.drawable.rd);

                break;

        }

    }
}
