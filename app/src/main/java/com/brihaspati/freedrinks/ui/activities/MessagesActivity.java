package com.brihaspati.freedrinks.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.brihaspati.freedrinks.R;
import com.brihaspati.freedrinks.pojo.MessagesPojo;
import com.brihaspati.freedrinks.ui.adapters.MessagesAdapter;
import com.brihaspati.freedrinks.ui.adapters.SocialPostsAdapter;
import com.brihaspati.freedrinks.ui.baseactivities.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vaneet on 3/27/17.
 */
public class MessagesActivity extends BaseActivity {

    Toolbar mToolbar;
    RecyclerView mRecyclerView;
    TextView mToolBarTittle;
     List<MessagesPojo> messagesList;
    MessagesAdapter adapter;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        setUpViews();
        setUpData();
    }

    @Override
    public void setUpViews() {

        try {
            mToolbar = (Toolbar)findViewById(R.id.toolbar);
            mToolBarTittle = (TextView)findViewById(R.id.text_toolbar_title);
            mRecyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        }catch (Exception e){
            e.getStackTrace();
        }

    }

    @Override
    public void setUpData() {


        try {
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
            mToolBarTittle.setText("Messages");

            messagesList = new ArrayList<>();

            adapter = new MessagesAdapter(messagesList);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(adapter);

            prepareData();

        }catch (Exception e){
            e.printStackTrace();
        }


    }

    @Override
    public void onClick(View v) {

    }

    private void prepareData(){

            for(int i = 0; i< 20 ; i++){

                MessagesPojo pojo = new MessagesPojo();
                pojo.setName("Martin");
                pojo.setTime("09:20");
                pojo.setText("Lorem text");

                messagesList.add(pojo);
            }

        adapter.notifyDataSetChanged();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case android.R.id.home:
                onBackPressed();
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
