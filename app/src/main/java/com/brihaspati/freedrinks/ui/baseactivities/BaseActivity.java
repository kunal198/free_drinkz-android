package com.brihaspati.freedrinks.ui.baseactivities;

import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by brst-pc97 on 3/23/17.
 */
public abstract class BaseActivity extends AppCompatActivity {


    public  abstract   void setUpViews();

    public  abstract  void  setUpData();


    public  abstract  void onClick(View v);

}
