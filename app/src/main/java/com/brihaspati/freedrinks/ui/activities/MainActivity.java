package com.brihaspati.freedrinks.ui.activities;

import android.content.Intent;
import   android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brihaspati.freedrinks.R;
import com.brihaspati.freedrinks.ui.baseactivities.BaseActivity;
import com.brihaspati.freedrinks.ui.fragments.ProfileFragment;

import java.util.Stack;

/**
 * Created by brst-pc97 on 3/23/17.
 */
public class MainActivity extends BaseActivity {

    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private NavigationView nvDrawer;

    private Stack<Fragment> fragmentStack;
    private FragmentManager fragmentManager;
    private ProfileFragment profileFragment;

    // Make sure to be using android.support.v7.app.ActionBarDrawerToggle version.
    // The android.support.v4.app.ActionBarDrawerToggle has been deprecated.
    private ActionBarDrawerToggle drawerToggle;

    LinearLayout layoutProfile, layoutContact, layoutMessages;
    TextView tittle;


    @Override
    protected void onCreate( Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        // Set a Toolbar to replace the ActionBar.
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Find our drawer view
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        nvDrawer = (NavigationView) findViewById(R.id.nvView);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = setupDrawerToggle();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
        drawerToggle.setDrawerIndicatorEnabled(false);
        drawerToggle.setHomeAsUpIndicator(R.drawable.menu);

        drawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        // Setup drawer view
//        setupDrawerContent(nvDrawer);
         mDrawer.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

        setUpViews();
        setUpData();
    }

    @Override
    public void setUpViews() {

        try {

            layoutProfile = (LinearLayout)nvDrawer.findViewById(R.id.layout_me);
            layoutContact = (LinearLayout)nvDrawer.findViewById(R.id.layout_contact);
            layoutMessages = (LinearLayout)nvDrawer.findViewById(R.id.layout_messages);

            tittle = (TextView)findViewById(R.id.text_toolbar_title);

            layoutProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (fragmentStack.size() > 0) {
                        FragmentTransaction ft = fragmentManager.beginTransaction();

                        if(profileFragment == null){
                            profileFragment = ProfileFragment.getInstance(MainActivity.this);
                            fragmentManager = getSupportFragmentManager();
                             ft = fragmentManager.beginTransaction();
                            ft.add(R.id.flContent, profileFragment);
                            fragmentStack.push(profileFragment);
                            ft.commit();
                        }else {

                            ft.show(profileFragment);
                            ft.commit();
                        }

                        tittle.setText("Profile");
                        mDrawer.closeDrawers();

                    }
                }
            });


            layoutContact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent I = new Intent(MainActivity.this, ContactActivity.class);
                    I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(I);

                    mDrawer.closeDrawers();
                }
            });


            layoutMessages.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent I = new Intent(MainActivity.this, MessagesActivity.class);
                    I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(I);

                    mDrawer.closeDrawers();
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void setUpData() {
        try {

            fragmentStack = new Stack<Fragment>();

            profileFragment =  ProfileFragment.getInstance(this);

            fragmentManager = getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.add(R.id.flContent, profileFragment);
            fragmentStack.push(profileFragment);
            ft.commit();
            tittle.setText("Profile");
        }catch (Exception e){
            e.getStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

//    private void setupDrawerContent(NavigationView navigationView) {
//        navigationView.setNavigationItemSelectedListener(
//                new NavigationView.OnNavigationItemSelectedListener() {
//                    @Override
//                    public boolean onNavigationItemSelected(MenuItem menuItem) {
//                        selectDrawerItem(menuItem);
//                        return true;
//                    }
//                });
//    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        // NOTE: Make sure you pass in a valid toolbar reference.  ActionBarDrawToggle() does not require it
        // and will not render the hamburger icon without it.
        return new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open,  R.string.drawer_close);
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        Fragment fragment = null;
        Class fragmentClass = null;
        switch(menuItem.getItemId()) {
            case R.id.nav_first_fragment:
//                fragmentClass = FirstFragment.class;
                break;
            case R.id.nav_second_fragment:
//                fragmentClass = SecondFragment.class;
                break;
            case R.id.nav_third_fragment:
//                fragmentClass = ThirdFragment.class;
                break;
            default:
//                fragmentClass = FirstFragment.class;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

        // Highlight the selected item has been done by NavigationView
        menuItem.setChecked(true);
        // Set action bar title
        setTitle(menuItem.getTitle());
        // Close the navigation drawer
        mDrawer.closeDrawers();
    }

    @Override
    public void onBackPressed() {

        if (fragmentStack.size() == 2) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            fragmentStack.lastElement().onPause();
            ft.remove(fragmentStack.pop());
            fragmentStack.lastElement().onResume();
            ft.show(fragmentStack.lastElement());
            ft.commit();
        } else {
            super.onBackPressed();
        }
    }



}
