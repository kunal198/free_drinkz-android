package com.brihaspati.freedrinks.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brihaspati.freedrinks.R;
import com.brihaspati.freedrinks.ui.baseactivities.BaseActivity;
import com.brihaspati.freedrinks.utilities.Utilites;

/**
 * Created by brst-pc97 on 3/24/17.
 */
public class LoginActivity extends BaseActivity {

    Toolbar mToolbarLogin;
    EditText mEdEmail, mEdPassword;
    Button mBtnSignIn;
    TextView mTvForgotPassword, mToolBarTittle;
    ImageView mIvFacebook, mIvGooglePlus, mIvTwitter;
    LinearLayout mLiCreateAccount;


    @Override
    protected void onCreate( Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setUpViews();
        setUpData();
    }

    @Override
    public void setUpViews() {

        try {
            mToolbarLogin = (Toolbar)findViewById(R.id.toolbar);

            mEdEmail    = (EditText)findViewById(R.id.edit_text_email);
            mEdPassword = (EditText)findViewById(R.id.edit_text_password);
            mToolBarTittle = (TextView)findViewById(R.id.text_toolbar_title);
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    @Override
    public void setUpData() {

        try {

            setSupportActionBar(mToolbarLogin);
            mToolBarTittle.setText("Login");

        }catch (Exception e){
            e.getStackTrace();
        }


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.btn_sign_in:
                Intent I = null;
                    if(Utilites.isInternetOn(this)){

                        I = new Intent(this, MainActivity.class);
                        I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(I);
                        finish();
                    }

                break;

            case R.id.text_forgot_password:

                if(Utilites.isInternetOn(this)){

                    Toast.makeText(this, "  Connected ", Toast.LENGTH_LONG).show();

                }

                break;


            case R.id.image_fb:

                if(Utilites.isInternetOn(this)){

                    Toast.makeText(this, "  Connected ", Toast.LENGTH_LONG).show();

                }

                break;

            case R.id.image_twitter:

                if(Utilites.isInternetOn(this)){

                    Toast.makeText(this, "  Connected ", Toast.LENGTH_LONG).show();


                }

                break;

            case R.id.image_google_plus:

                if(Utilites.isInternetOn(this)){

                    Toast.makeText(this, "  Connected ", Toast.LENGTH_LONG).show();


                }

                break;

            case R.id.layout_create_account:

                if(Utilites.isInternetOn(this)){
                    I = new Intent(this, SignUpActivity.class);
                    I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(I);
                }

                break;

        }

    }
}
